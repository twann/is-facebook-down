# Is Facebook down?

![image](image.png)

Check if facebook is down every 30 minutes

## Usage

### Check every 30 minutes

```sh
./is-facebook-down.sh
```

### Check every minute

```sh
cat one-minute.patch| patch
./is-facebook-down.sh
```

## License

This script is released under [the Unlicense](https://unlicense.org/).

