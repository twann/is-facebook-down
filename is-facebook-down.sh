#!/bin/sh

while true; do
	if [ "$(ping -c1 facebook.com&>/dev/null)" ]; then
		echo -e "\033[91;1mFacebook is back :'(\033[0m"
		break
	else
		echo -e "\033[92;1mFacebook is down xD\033[0m"
	fi
	echo -e "\033[37;1mLet's check again in 30 minutes...\033[0m"
	sleep 1800
done
